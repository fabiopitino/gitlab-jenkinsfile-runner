# frozen_string_literal: true

module JenkinsfileRunner
  module Commands
    class Build
      class Configuration < ::JenkinsfileRunner::BaseConfiguration
        config_attributes %i[
          registry
          image
          build_output
        ]

        def validate
          @errors = []
          @errors.concat(validate_presence(all_attributes))
          @errors.concat(validate_paths(%i[build_output]))
        end

        private

        def parser
          @parser ||= OptionParser.new do |option|
            option.banner = 'Usage: build [options]'

            option.on '--registry registry.gitlab.com'
            option.on '--image myGitLabProject/jenkins-importer'
            option.on '--build-output /path/to/tmp/work/dir'

            option.on('-h', '--help', 'Prints this help') do
              puts option
              exit
            end
          end
        end
      end
    end
  end
end
