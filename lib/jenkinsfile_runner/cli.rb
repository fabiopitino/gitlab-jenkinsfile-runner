# frozen_string_literal: true

module JenkinsfileRunner
  class CLI
    attr_reader :command

    def initialize(argv)
      @command = Command.build(argv.dup)
    end

    def run
      command.run
    end
  end
end
