# frozen_string_literal: true

require_relative 'commands/help'
require_relative 'commands/init'
require_relative 'commands/build'

module JenkinsfileRunner
  module Command
    COMMANDS = {
      'init' => Commands::Init,
      'build' => Commands::Build
    }.freeze

    module_function

    def build(argv)
      command_class_for(argv.shift).new(argv)
    end

    def command_class_for(name)
      COMMANDS.fetch(name) { Commands::Help }
    end
  end
end
