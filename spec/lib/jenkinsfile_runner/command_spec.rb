require 'spec_helper'

RSpec.describe JenkinsfileRunner::Command do
  describe '.build' do
    subject(:subject) { described_class.build(argv) }

    context 'with registered commands' do
      let(:argv) { ['init'] }

      it { is_expected.to be_a(JenkinsfileRunner::Commands::Init) }
    end

    context 'without registered commands' do
      let(:argv) { ['bacon'] }

      it { is_expected.to be_a(JenkinsfileRunner::Commands::Help) }
    end
  end
end
