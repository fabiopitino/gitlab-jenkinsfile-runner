require 'spec_helper'

RSpec.describe JenkinsfileRunner::Commands::Init do
  subject(:subject) { described_class.new([]) }

  it { is_expected.to respond_to(:run) }
end
