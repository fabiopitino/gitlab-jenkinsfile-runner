require 'spec_helper'
require 'securerandom'

RSpec.describe JenkinsfileRunner::Commands::Init do
  subject(:init) { described_class.new(argv) }
  let(:argv) { [] }

  it { is_expected.to respond_to(:run) }

  context 'with known arguments' do
    let(:argv) do
      [
        '--jfr-version', '1.2.3',
        '--jenkins-home', '/var/jenkins_home',
        '--jenkins-war', '/usr/share/jenkins/jenkins.war',
        '--agent-type', 'docker',
        '--build-output', 'path/to/output'
      ]
    end

    it 'parses configuration' do
      expect(subject.configuration)
        .to have_attributes(
          jfr_version: '1.2.3',
          jenkins_home: '/var/jenkins_home',
          jenkins_war: '/usr/share/jenkins/jenkins.war',
          agent_type: :docker,
          build_output: 'path/to/output')
    end
  end

  context 'with unknown arguments' do
    let(:argv) do
      ['--errors', 'no']
    end

    it 'raises an exception' do
      expect { subject}.to raise_error(OptionParser::InvalidOption)
    end
  end

  describe '#run' do
    describe 'output directory' do
      subject(:create_output_directory) { init.send(:create_output_directory) }

      let(:path) { Pathname("/tmp/#{SecureRandom.uuid}") }
      let(:argv) { [ '--build-output', path.to_s ] }

      after do
        path.unlink if path.exist?
      end

      it 'creates the output directory' do
        expect { create_output_directory }.to change { path.exist? }
      end
    end

    describe '/app/bin/jenkinsfile-runner' do
      let(:path) { Pathname("/tmp/#{SecureRandom.uuid}").tap(&:mkpath) }
      let(:destination_file) { path.join('jenkinsfile-runner') }
      let(:argv) { [ '--build-output', path.to_s ] }

      subject(:create_jenkinsfile_runner_executable) do
        init.send(:create_jenkinsfile_runner_executable)
      end

      after do
        destination_file.unlink if destination_file.exist?
        path.unlink if path.exist?
      end

      it 'creates jenkinsfile-runner file' do
        expect { create_jenkinsfile_runner_executable }.to change { destination_file.exist? }
      end
    end

    describe '/app/jenkins_home' do
      subject(:copy_jenkins_home_directory) { init.send(:copy_jenkins_home_directory) }

      let(:home_path) { Pathname("/tmp/#{SecureRandom.uuid}").tap(&:mkpath) }
      let(:out_path) { Pathname("/tmp/#{SecureRandom.uuid}").tap(&:mkpath) }
      let(:argv) { [ '--jenkins-home', home_path.to_s, '--build-output', out_path.to_s ] }

      after do
        home_path.unlink if home_path.exist?
        out_path.rmtree if out_path.exist?
      end

      it 'copies the home directory' do
        expect { copy_jenkins_home_directory }.to change { out_path.join('jenkins_home').exist? }
      end
    end

    describe '/app/bin/jenkins.war' do
      subject(:copy_jenkins_war_file) { init.send(:copy_jenkins_war_file) }

      let(:home_path) { Pathname("/tmp/#{SecureRandom.uuid}").tap(&:mkpath) }
      let(:war_path) { home_path.join('jenkins.war').tap { |path| path.write('test') } }
      let(:out_path) { Pathname("/tmp/#{SecureRandom.uuid}").tap(&:mkpath) }
      let(:argv) { [ '--jenkins-war', war_path.to_s, '--build-output', out_path.to_s ] }

      after do
        home_path.rmtree if home_path.exist?
        out_path.rmtree if out_path.exist?
      end

      it 'copies the home directory' do
        expect { copy_jenkins_war_file }.to change { out_path.join('jenkins.war').exist? }
      end
    end

    describe 'Dockerfile' do
      let(:path) { Pathname("/tmp/#{SecureRandom.uuid}").tap(&:mkpath) }
      let(:dockerfile_path) { path.join('Dockerfile') }
      let(:argv) { [ '--build-output', path.to_s, '--jfr-version', '1.2.3' ] }

      subject(:create_docker_file) do
        init.send(:create_docker_file)
      end

      after do
        dockerfile_path.unlink if dockerfile_path.exist?
        path.unlink if path.exist?
      end

      it 'creates Dockerfile' do
        expect { create_docker_file }.to change { dockerfile_path.exist? }
      end

      context 'with --agent-type docker' do
        let(:argv) { ['--build-output', path.to_s, '--agent-type', 'docker', '--jfr-version', '1.2.3'] }

        it 'creates jenkinsfile-runner file' do
          create_docker_file
          expect(dockerfile_path.read).to include('apt install -y docker-ce-cli')
        end
      end

      context 'with --jfr-version 1.2.3' do
        let(:argv) { ['--build-output', path.to_s, '--jfr-version', '1.2.3'] }

        it 'creates jenkinsfile-runner file' do
          create_docker_file
          expect(dockerfile_path.read).to include('jenkinsfile-runner/1.2.3/jenkinsfile-runner-1.2.3.jar')
        end
      end
    end

    describe 'ignore files' do
      let(:out_path) { Pathname("/tmp/#{SecureRandom.uuid}").tap(&:mkpath) }
      let(:argv) { ['--build-output', out_path.to_s] }

      subject(:create_ignore_files) do
        init.send(:create_ignore_files)
      end

      after do
        out_path.rmtree if out_path.exist?
      end

      it 'creates a dockerignore file' do
        expect { create_ignore_files }.to change { out_path.join('.dockerignore').exist? }
      end

      it 'creates a gitignore file' do
        expect { create_ignore_files }.to change { out_path.join('.gitignore').exist? }
      end
    end
  end
end
